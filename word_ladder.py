import re


def same(item, target):     ##Function to return the numbers of letter that are also in the target word.
  return len([c for (c, t) in zip(item, target) if c == t])

def build(pattern, words, seen, list):
  ## Builds a list of words by replacing each letter of the word one at
  # a time.
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list]

def find(word, words, seen, target, path, short):
  ##Builds a list named path by selecting words from the list created with the build function. It selects the word in
  # list with the highest number of letters that exists in the target word and repeats until it reaches the target
  #  word and then returns the shortest path
  list = []
  bad_path = ["x" , "z"]
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list)
  if len(list) == 0:
    return False
  if short == True:
    list = sorted([(same(w, target), w) for w in list], reverse = True)
  else:
    list = sorted([(same(w, target), w) for w in list])
  for (match, item) in list:
    for x in bad_path:              #avoid routes using x and z letters helps get correct result for lead to gold in 6 steps
      if x in item:
        list.remove((match, item))
    if match >= len(target) - 1:
      if match == len(target) - 1:
        path.append(item)
      return True
    seen[item] = True
  for (match, item) in list:
    path.append(item)
    if find(item, words, seen, target, path, short):
      return True
    path.pop()

def input_check(start):     ##Check if the start word contains any digits is blank or has any special characters
  if start == "" or not start.isalnum():
    return False
  if any(char.isdigit() for char in start):
    return False
  else:
    return True

def input_check_target(target, start):
  ##Check if the goal word contains any digits is blank or has any special
  # characters and is same length of the start word
  if target == "" or not target.isalnum()or len(target) != len(start):
    return False
  if any(char.isdigit() for char in target):
    return False
  else:
    return True


def file_check(dictionary):
  while FileNotFoundError:
    ## Ensure's the user has entered a correct file name. If it is not Found gives and error and tries again
    try:
      file = open(dictionary)
      global lines
      lines = file.readlines()
      break
    except FileNotFoundError:
      print ("\nThe file name you entered cannot be found\n")
      dictionary = (input("Enter dictionary name: "))

def main():

  file_check(input("Enter dictionary name: "))

  while True:
    start = input("Enter start word:").replace (" ", "")      ##remove any spaces that may of been included
    while input_check(start) == False:
      print ("\nYour input is invalid. Please ensure input is not blank and does not contain any digits or other special characters\n")
      start = input("Enter start word:").replace(" ", "")


    words = []
    for line in lines:
      word = line.rstrip()
      if len(word) == len(start):
        words.append(word)

    target = input("Enter target word:").replace(" ", "")
    while input_check_target(target, start) == False:
      print("\nYour input is invalid. Please ensure input is not blank and does not contain any digits or other special characters and is of the same length as the start word\n")
      target = input("Enter target word:").replace(" ", "")
    break

  short = bool(input("If you would like the shortest path please type yes otherwise for the longer path just press enter: "))


  path = [start]
  seen = {start : True}
  if find(start, words, seen, target, path, short):
    path.append(target)
    print(len(path) - 1, path)
  else:
    print("No path found")

if __name__ == '__main__':
    main()