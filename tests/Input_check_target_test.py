import unittest
from word_ladder import input_check_target

class Test_target_check(unittest.TestCase):


   def  test_self_check(self):
       self.assertEqual(input_check_target('', 'hide'), False)
       self.assertEqual(input_check_target('35', 'hide'), False)
       self.assertEqual(input_check_target(' ', 'hide'), False)
       self.assertEqual(input_check_target('@ide', 'hide'), False)
       self.assertEqual(input_check_target('seek', 'hide'), True)
       self.assertEqual(input_check_target('lead', 'gold'), True)

if __name__ == '__main__':
    unittest.main()